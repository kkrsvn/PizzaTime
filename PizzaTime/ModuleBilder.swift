//
//  ModuleBilder.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit
import CoreData

protocol MainModuleBilderProtocol {
    static func createMainModule() -> UIViewController
}

class MainModuleBilder: MainModuleBilderProtocol {
    
    static func createMainModule() -> UIViewController {
        
        let view = MainViewController(bannerDataSource: bannerData, categoriesDataSource: categoriesLablesData)
        let networkService = NetwortService()
        let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext
        let presenter = MainPresenter(view: view, networkService: networkService)
        presenter.context = context
        view.presenter = presenter
        return view
    }
}
