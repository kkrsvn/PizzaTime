//
//  MainPresenter.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit
import CoreData

protocol MainViewProtocol {
    func succes()
}

protocol MainPresenterProtocol {
    var pizzaDataSource: RecipesModel? { get set }
    var dessertDataSource: RecipesModel? { get set }
    var pizzaReservedData: RecipesModel? { get set }
    var dessertReservedData: RecipesModel? { get set }
    var faildLoadData: Bool? { get set }
    var mainView: MainViewProtocol { get set }
    var networkService: NetworkServiceProtocol { get set }
    func getRecipes()
    init(view: MainViewProtocol, networkService: NetworkServiceProtocol)
}

class MainPresenter: MainPresenterProtocol {
    
    var pizzaReservedData: RecipesModel?
    var dessertReservedData: RecipesModel?
    var context: NSManagedObjectContext!
    
    var faildLoadData: Bool? {
        didSet {
            self.pizzaDataSource = self.pizzaReservedData
            self.dessertDataSource = self.dessertReservedData
        }
    }
    
    var pizzaDataSource: RecipesModel? {
        didSet {
            DispatchQueue.main.async {
                self.mainView.succes()
            }
            self.saveCoreData()
        }
    }
    var dessertDataSource: RecipesModel? {
        didSet {
            DispatchQueue.main.async {
                self.mainView.succes()
            }
            self.saveCoreData()
        }
    }
    var mainView: MainViewProtocol
    var networkService: NetworkServiceProtocol

    required init(view: MainViewProtocol, networkService: NetworkServiceProtocol) {
        self.mainView = view
        self.networkService = networkService
    }
    
    func getRecipes() {
        
        self.loadCoreData()
        
        self.networkService.onFailed = {[weak self] viewData in
            self?.faildLoadData = viewData
        }
        
        self.networkService.fetchRecipes(forRequestType: .pizza)
        self.networkService.onComplitionPizza = { [weak self] viewData in
            self?.pizzaDataSource = viewData
        }
        self.networkService.fetchRecipes(forRequestType: .dessert)
        self.networkService.onComplitionDessert = { [weak self] viewData in
            self?.dessertDataSource = viewData
        }
    }
    
    private func loadCoreData() {
        let fetchRequestPizza: NSFetchRequest<Pizza> = Pizza.fetchRequest()
        let fetchRequestDessert: NSFetchRequest<Dessert> = Dessert.fetchRequest()

        do {
            let pizzaContextData: [Pizza] = try self.context.fetch(fetchRequestPizza)
            let dessertContextData: [Dessert] = try self.context.fetch(fetchRequestDessert)
            
            var dataPizza: [(title : String, imageURL : String, comment: String)] = []
            var dataDessert: [(title : String, imageURL : String, comment: String)] = []
            
            for pizza in pizzaContextData {
                dataPizza.append((title: (pizza as Pizza).title ?? "",
                             imageURL: (pizza as Pizza).imageURL ?? "",
                             comment: (pizza as Pizza).comment ?? ""))
            }
            
            for dessert in dessertContextData {
                dataDessert.append((title: (dessert as Dessert).title ?? "",
                             imageURL: (dessert as Dessert).imageURL ?? "",
                             comment: (dessert as Dessert).comment ?? ""))
            }
            
            self.pizzaReservedData = RecipesModel(data: dataPizza)
            self.dessertReservedData = RecipesModel(data: dataDessert)
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    private func saveCoreData() {
        guard let pizzaData = self.pizzaDataSource?.recipes else { return }
        guard let dessertData = self.dessertDataSource?.recipes else { return }
        
        for pizza in pizzaData {
            guard let entity = NSEntityDescription.entity(forEntityName: "Pizza", in: context) else { return }
            let pizzaObject = Pizza(entity: entity, insertInto: context)
            pizzaObject.title = pizza.title
            pizzaObject.imageURL = pizza.imageURL
            pizzaObject.comment = pizza.comment
        }
        
        for dessert in dessertData {
            guard let entity = NSEntityDescription.entity(forEntityName: "Dessert", in: context) else { return }
            let dessertObject = Dessert(entity: entity, insertInto: context)
            dessertObject.title = dessert.title
            dessertObject.imageURL = dessert.imageURL
            dessertObject.comment = dessert.comment
        }
        
        do {
            try self.context.save()
            self.pizzaReservedData = self.pizzaDataSource
            self.dessertReservedData = self.dessertDataSource
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
