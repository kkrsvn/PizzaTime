//
//  NetworkService.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

enum RequestType {
    case pizza
    case dessert
    
    var name: String {
        switch self {
        case .pizza:
            return "pizza"
        case .dessert:
            return "dessert"
        }
    }
}

protocol NetworkServiceProtocol: AnyObject {
    var onComplitionPizza: ((RecipesModel) -> Void)? { get set }
    var onComplitionDessert: ((RecipesModel) -> Void)? { get set }
    var onFailed: ((Bool) -> Void)? { get set }
    func fetchRecipes(forRequestType requestType: RequestType)
}

class NetwortService: NetworkServiceProtocol {
    
    var onComplitionPizza: ((RecipesModel) -> Void)?
    var onComplitionDessert: ((RecipesModel) -> Void)?
    var onFailed: ((Bool) -> Void)?
    
    func fetchRecipes(forRequestType requestType: RequestType) {
        let urlString = "https://api.spoonacular.com/recipes/complexSearch?apiKey=\(apiKey2)&query=\(requestType.name)&number=5&fillIngredients=true"
        performRequest(withURLString: urlString, forRequestType: requestType)
    }
    
    fileprivate func performRequest(withURLString urlString: String, forRequestType requestType: RequestType) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, respounse, error in
            if let data = data {
                if let recipesModel = self.parseJSON(withData: data) {
                    switch requestType {
                    case .pizza:
                        self.onComplitionPizza?(recipesModel)
                    case .dessert:
                        self.onComplitionDessert?(recipesModel)
                    }
                }
            } else {
                self.onFailed?(true)
            }
        }
        task.resume()
    }
    
    fileprivate func parseJSON(withData data: Data) -> RecipesModel? {
        let decoder = JSONDecoder()
        do {
            let recipesData = try decoder.decode(RecipesData.self, from: data)
            guard let recipesModel = RecipesModel(recipesData: recipesData) else {
                return nil
            }
            return recipesModel
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
