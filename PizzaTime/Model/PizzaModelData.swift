//
//  PizzaModelData.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import Foundation

// MARK: - Recipes
struct RecipesData: Codable {
    let results: [Result]
}

// MARK: - Result
struct Result: Codable {
    let missedIngredients: [MissedIngredient]
    let title: String
    let image: String
}

// MARK: - MissedIngredient
struct MissedIngredient: Codable {
    let name: String
}

// MARK: - Recipes Model
struct RecipesModel {
    var recipes: [(title : String, imageURL : String, comment: String)] = []
    
    init?(recipesData: RecipesData) {
        for element in recipesData.results {
            let title = element.title
            let imageURL = element.image
            var ingredients: [String] = []
            
            for ingredient in element.missedIngredients {
                ingredients.append(ingredient.name)
            }
            
            let comment: String = ingredients.joined(separator: ", ")
            
            self.recipes.append((title : title, imageURL : imageURL, comment: comment))
        }
    }
    
    init?(data: [(title : String, imageURL : String, comment: String)]) {
        for element in data {
            self.recipes.append((title: element.title,
                                 imageURL: element.imageURL,
                                 comment: element.comment))
        }
    }
}
