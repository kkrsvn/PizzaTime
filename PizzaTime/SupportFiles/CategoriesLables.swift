//
//  CategoriesLables.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import Foundation

let categoriesLablesData: [String] = ["Пицца", "Десерты", "Комбо", "Суши", "Соусы", "Напитки"]
