//
//  MVTableDS.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

extension MainViewController: UITableViewDataSource {
    
    // MARK: Количество секций
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // MARK: Количество ячеек в секциях
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.presenter.pizzaDataSource?.recipes.count ?? 5
        } else {
            return self.presenter.dessertDataSource?.recipes.count ?? 5
        }
    }
    
    // MARK: Заполнение ячеек
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var title: String
        var image: String
        var comment: String
        
        if indexPath.section == 0 {
            title = self.presenter.pizzaDataSource?.recipes[indexPath.row].title ?? "404"
            image = self.presenter.pizzaDataSource?.recipes[indexPath.row].imageURL ?? "404"
            comment = "Состав: " + (self.presenter.pizzaDataSource?.recipes[indexPath.row].comment ?? "")
        } else {
            title = self.presenter.dessertDataSource?.recipes[indexPath.row].title ?? "404"
            image = self.presenter.dessertDataSource?.recipes[indexPath.row].imageURL ?? "404"
            comment = "Состав: " + (self.presenter.dessertDataSource?.recipes[indexPath.row].comment ?? "")
        
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecipesCell", for: indexPath) as? RecipesCell else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultTVCell", for: indexPath)
            cell.backgroundColor = .red
            return cell
        }
        
        let viewModel = RecipesCell.Recipes(title: title, comment: comment, picture: image)
        
        cell.setup(with: viewModel)
        
        return cell
    }
}
