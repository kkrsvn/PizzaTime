//
//  MainViewController.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 03.04.2023.
//

import UIKit

class MainViewController: UIViewController {
    
    var bannerDataSource: [String]
    var categoriesDataSource: [String]
    
    var presenter: MainPresenterProtocol!
    var mainScrollView: UIScrollView!
    var bannerCollectionView: UICollectionView!
    var categoriesCollectionView: UICollectionView!
    var mainTableView: UITableView!
    var locationLable: UILabel!
    var locationIcon: UIImageView!
    
    init(bannerDataSource: [String], categoriesDataSource: [String]) {
        self.bannerDataSource = bannerDataSource
        self.categoriesDataSource = categoriesDataSource
        super .init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        self.presenter.getRecipes()
    }
    
    private func setUpView() {
        self.view.backgroundColor = UIColor(red: 0.953, green: 0.961, blue: 0.976, alpha: 1)
        self.locationLable = self.makeLocationLable()
        self.locationIcon = self.makeLocationIcon()
        self.mainScrollView = self.makeScrollView()
        self.bannerCollectionView = self.makeBannerCollectionView()
        self.mainTableView = self.makeTableView()
        self.categoriesCollectionView = self.makeCategoriesCollection()
        NSLayoutConstraint.activate(self.activeCategoriesCollectionConstants)
    }
    
    private lazy var activeCategoriesCollectionConstants = [
        self.categoriesCollectionView.topAnchor.constraint(equalTo: self.bannerCollectionView.bottomAnchor, constant: 24),
        self.categoriesCollectionView.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0),
        self.categoriesCollectionView.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 0),
        self.categoriesCollectionView.heightAnchor.constraint(equalToConstant: 40)
    ]
    
    private lazy var staticCategoriesCollectionConstants = [
        self.categoriesCollectionView.topAnchor.constraint(equalTo: self.locationLable.bottomAnchor, constant: 24),
        self.categoriesCollectionView.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0),
        self.categoriesCollectionView.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 0),
        self.categoriesCollectionView.heightAnchor.constraint(equalToConstant: 50)
    ]
    
    func changeScrollViewPosition(at y: CGFloat) {
        self.mainScrollView.contentOffset.y = y
    }

}

extension MainViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 132 {
            NSLayoutConstraint.deactivate(self.activeCategoriesCollectionConstants)
            NSLayoutConstraint.activate(self.staticCategoriesCollectionConstants)
        } else {
            NSLayoutConstraint.deactivate(self.staticCategoriesCollectionConstants)
            NSLayoutConstraint.activate(self.activeCategoriesCollectionConstants)
        }
    }
}

extension MainViewController: MainViewProtocol {
    func succes() {
        self.mainScrollView.contentSize.height = CGFloat(200 + 172 * ((self.presenter.pizzaDataSource?.recipes.count ?? 0) + (self.presenter.dessertDataSource?.recipes.count ?? 0)))
        self.mainTableView.reloadData()
    }
}
