//
//  MVCollectionDS.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 03.04.2023.
//

import UIKit

extension MainViewController: UICollectionViewDataSource {
    
    // MARK: Количество ячеек в коллекциях
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.bannerCollectionView {
            return self.bannerDataSource.count
        } else {
            return self.categoriesDataSource.count
        }
    }
    
    // MARK: Заполнение коллекций
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.bannerCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCell", for: indexPath) as? BannerCell else  {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCVCell", for: indexPath)
                cell.backgroundColor = .gray
                return cell
            }
            let image = self.bannerDataSource[indexPath.row]
            let viewModel = BannerCell.Banner(image: image)
            cell.setup(with: viewModel)
            return cell
            
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as? CategoriesCell else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCVCell", for: indexPath)
                cell.backgroundColor = .red
                return cell
            }
            let lable = self.categoriesDataSource[indexPath.row]
            let viewModel = CategoriesCell.Categories(lable: lable)
            cell.setup(with: viewModel)
            return cell
        }
    }
}


