//
//  TabBar.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

class TabBarController: UITabBarController {
    
    private enum TabBarItem {
        case menu
        case contacts
        case profile
        case basket
        
        
        var title: String {
            switch self {
            case .menu:
                return "Меню"
            case .contacts:
                return "Контакты"
            case .profile:
                return "Профиль"
            case .basket:
                return "Корзина"
            }
        }
        
        var image: UIImage? {
            switch self {
            case .menu:
                return UIImage(systemName: "fork.knife.circle", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 20)))
            case .contacts:
                return UIImage(systemName: "list.bullet.clipboard", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 20)))
            case .profile:
                return UIImage(systemName: "person.crop.circle", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 20)))
            case .basket:
                return UIImage(systemName: "basket", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 20)))
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.changeHeightOfTabbar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        self.setUpTabBar()
    }
    
    private func setUpView() {
        self.tabBar.tintColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 1)
        self.tabBar.unselectedItemTintColor = UIColor(red: 0.765, green: 0.77, blue: 0.788, alpha: 1)
        self.tabBar.backgroundColor = .white
        self.changeRadiusOfTabbar()
    }
    
    private func changeHeightOfTabbar(){
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = 83
        tabFrame.origin.y = self.view.frame.size.height - tabFrame.size.height
        self.tabBar.frame = tabFrame
    }
    
    private func changeRadiusOfTabbar(){
        self.tabBar.layer.masksToBounds = true
        self.tabBar.isTranslucent = true
        self.tabBar.layer.borderWidth = 1
        self.tabBar.layer.borderColor = UIColor(red: 0.765, green: 0.77, blue: 0.788, alpha: 1).cgColor
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func setUpTabBar() {
        let items: [TabBarItem] = [.menu, .contacts, .profile, .basket]
        
        self.viewControllers = items.map( { TabBarItem in
            switch TabBarItem {
            case .menu:
                return MainModuleBilder.createMainModule()
            case .contacts:
                return UIViewController()
            case .profile:
                return UIViewController()
            case .basket:
                return UIViewController()
            }
        })
        
        self.viewControllers?.enumerated().forEach( { (index, viewController) in
            viewController.tabBarItem.title = items[index].title
            viewController.tabBarItem.image = items[index].image
        })
    }
}
