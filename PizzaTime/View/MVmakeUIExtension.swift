//
//  MainViewExtension.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 03.04.2023.
//

import UIKit

extension MainViewController {
    
    func makeLocationLable() -> UILabel {
        let lable = UILabel()
        lable.text = "Москва"
        lable.font = UIFont(name: "SFUIDisplay-Medium", size: 17)
        lable.textColor = UIColor(red: 0.133, green: 0.157, blue: 0.192, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(lable)
    
        lable.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        lable.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        return lable
    }
    
    func makeLocationIcon() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "locationIcon")
                                  
        self.view.addSubview(imageView)
        
        imageView.leadingAnchor.constraint(equalTo: self.locationLable.trailingAnchor, constant: 8).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.locationLable.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 8).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 14).isActive = true
                                  
        return imageView
    }
    
    func makeScrollView() -> UIScrollView {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.bounces = false
        scroll.contentSize.height = 216
        scroll.delegate = self
        scroll.backgroundColor = UIColor(red: 0.953, green: 0.961, blue: 0.976, alpha: 1)
        
        self.view.addSubview(scroll)
    
        scroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scroll.topAnchor.constraint(equalTo: self.locationLable.bottomAnchor, constant: 24).isActive = true
        scroll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,constant: -83).isActive = true
        
        return scroll
    }
    
    func makeBannerCollectionView() -> UICollectionView {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 16
        layout.itemSize = CGSize(width: 300, height: 112)
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCVCell")
        collection.register(BannerCell.self, forCellWithReuseIdentifier: "BannerCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.mainScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.mainScrollView.topAnchor).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 16).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 112).isActive = true
        
        return collection
    }
    
    func makeCategoriesCollection() -> UICollectionView {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 8
        layout.itemSize = CGSize(width: 88, height: 32)
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.dataSource = self
        collection.delegate = self
        collection.backgroundColor = UIColor(red: 0.953, green: 0.961, blue: 0.976, alpha: 1)
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCVCell")
        collection.register(CategoriesCell.self, forCellWithReuseIdentifier: "CategoriesCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.mainScrollView.addSubview(collection)
        
        return collection
    }
    
    func makeTableView() -> UITableView {
        let tableView = UITableView()
        tableView.backgroundColor = .gray
        tableView.clipsToBounds = true
        tableView.layer.cornerRadius = 20
        tableView.showsVerticalScrollIndicator = false
        tableView.isScrollEnabled = false
        tableView.bounces = false
        tableView.rowHeight = 172
        tableView.dataSource = self
        tableView.register(RecipesCell.self, forCellReuseIdentifier: "RecipesCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultTVCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.mainScrollView.addSubview(tableView)

        tableView.topAnchor.constraint(equalTo: self.mainScrollView.bottomAnchor, constant: 200).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.mainScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.mainScrollView.leadingAnchor, constant: 0).isActive = true
        tableView.heightAnchor.constraint(equalToConstant: 172 * 20).isActive = true
        
        return tableView
    }
}
