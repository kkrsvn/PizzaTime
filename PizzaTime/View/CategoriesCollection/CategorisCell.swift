//
//  CategorisCell.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

protocol CategoryViewCellProtocol {
    var lable: String { get set }
}

protocol CategoriesSetupable {
    func setup(with viewModel: CategoryViewCellProtocol)
}

protocol CellActivationProtocol {
    func changeIconColor(forChange: Status)
}

enum Status {
    case active
    case passive
}

class CategoriesCell: UICollectionViewCell {
    
    struct Categories: CategoryViewCellProtocol {
        var lable: String
    }
    
    private lazy var categoryLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "SFUIDisplay-Bold", size: 13)
        lable.textColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 0.4)
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var lableConstraints = [
        self.categoryLable.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
        self.categoryLable.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    private func setupView() {
        self.contentView.addSubview(self.categoryLable)
        self.setupConstraints()
        self.contentViewSets()
    }
    
    private func contentViewSets() {
        self.contentView.backgroundColor = .white
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 16
        self.contentView.layer.borderWidth = 1
        self.contentView.layer.borderColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 0.4).cgColor
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate(self.lableConstraints)
    }
}

extension CategoriesCell: CategoriesSetupable {
    func setup(with viewModel: CategoryViewCellProtocol) {
        guard let model = viewModel as? Categories else { return }
        self.categoryLable.text = model.lable
    }
}

extension CategoriesCell: CellActivationProtocol {
    func changeIconColor(forChange: Status) {
        switch forChange {
        case .active:
            self.contentView.backgroundColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 0.2)
            self.categoryLable.textColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 1)
        case .passive:
            self.contentView.backgroundColor = .white
            self.categoryLable.textColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 0.4)
        }
    }
}
