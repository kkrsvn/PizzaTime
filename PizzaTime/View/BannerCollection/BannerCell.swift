//
//  BannerCell.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

protocol BannerViewCellProtocol {
    var image: String { get set }
}

protocol BannerSetupable {
    func setup(with viewModel: BannerViewCellProtocol)
}

class BannerCell: UICollectionViewCell {
    
    struct Banner: BannerViewCellProtocol {
        var image: String
    }
    
    private lazy var mainImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var mainImageConstraints = [
        self.mainImage.topAnchor.constraint(equalTo: self.contentView.topAnchor),
        self.mainImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
        self.mainImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
        self.mainImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.mainImage)
        self.setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.mainImageConstraints)
    }
}

extension BannerCell: BannerSetupable {
    func setup(with viewModel: BannerViewCellProtocol) {
        guard let model = viewModel as? Banner else { return }
        self.mainImage.image = UIImage(named: model.image)
    }
}
