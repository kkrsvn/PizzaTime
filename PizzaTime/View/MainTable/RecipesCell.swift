//
//  RecipesCell.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

protocol RecipesCellProtocol {
    var title: String { get set }
    var comment: String { get set }
    var picture: String { get set }
}

protocol RecipesCellSetupableProtocol {
    func setup(with viewModel: RecipesCellProtocol)
}

class RecipesCell: UITableViewCell {
    
    struct Recipes: RecipesCellProtocol {
        var title: String
        var comment: String
        var picture: String
    }
    
    // MARK: Background Image
    private lazy var mainImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 132 / 2
        return imageView
    }()
    
    private lazy var mainImageConstraints = [
        self.mainImage.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
        self.mainImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
        self.mainImage.heightAnchor.constraint(equalToConstant: 132),
        self.mainImage.widthAnchor.constraint(equalToConstant: 132)
    ]
    
    // MARK: Background Price ImageView
    private lazy var priceBackgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .white
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 6
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 1).cgColor
        return imageView
    }()
    
    private lazy var priceBackgroundImageConstraints = [
        self.priceBackgroundImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -16),
        self.priceBackgroundImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16),
        self.priceBackgroundImage.heightAnchor.constraint(equalToConstant: 32),
        self.priceBackgroundImage.widthAnchor.constraint(equalToConstant: 87)
    ]
    
    // MARK: Price Lable
    private lazy var priceLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "SFUIDisplay-Thin", size: 13)
        lable.textColor = UIColor(red: 0.992, green: 0.227, blue: 0.412, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var priceLableConstraints = [
        self.priceLable.centerYAnchor.constraint(equalTo: self.priceBackgroundImage.centerYAnchor),
        self.priceLable.centerXAnchor.constraint(equalTo: self.priceBackgroundImage.centerXAnchor)
    ]
    
    // MARK: Title Lable
    private lazy var titleLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "SFUIDisplay-Semibold", size: 17)
        lable.textColor = UIColor(red: 0.133, green: 0.157, blue: 0.192, alpha: 1)
        lable.numberOfLines = 2
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var titleLableConstraints = [
        self.titleLable.topAnchor.constraint(equalTo: self.mainImage.topAnchor, constant: 0),
        self.titleLable.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -24),
        self.titleLable.leadingAnchor.constraint(equalTo: self.mainImage.trailingAnchor, constant: 32)
    ]
    
    // MARK: Ingredien Lable
    private lazy var ingredientsLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "SFUIDisplay-Thin", size: 13)
        lable.textColor = UIColor(red: 0.665, green: 0.668, blue: 0.679, alpha: 1)
        lable.numberOfLines = 4
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var ingredientsLableConstraints = [
        self.ingredientsLable.topAnchor.constraint(equalTo: self.titleLable.bottomAnchor, constant: 8),
        self.ingredientsLable.leadingAnchor.constraint(equalTo: self.titleLable.leadingAnchor, constant: 0),
        self.ingredientsLable.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -24),
    ]
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.mainImage)
        self.contentView.addSubview(self.priceBackgroundImage)
        self.contentView.addSubview(self.priceLable)
        self.contentView.addSubview(self.titleLable)
        self.contentView.addSubview(self.ingredientsLable)
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.mainImageConstraints)
        NSLayoutConstraint.activate(self.priceBackgroundImageConstraints)
        NSLayoutConstraint.activate(self.priceLableConstraints)
        NSLayoutConstraint.activate(self.titleLableConstraints)
        NSLayoutConstraint.activate(self.ingredientsLableConstraints)
    }
}

extension RecipesCell: RecipesCellSetupableProtocol {
    func setup(with resipesViewModel: RecipesCellProtocol) {
        guard let model = resipesViewModel as? Recipes else { return }
        
        self.priceLable.text = "от " + String(Int.random(in: 300...400)) + " р"
        self.titleLable.text = model.title
        self.ingredientsLable.text = model.comment

        guard let pictureURL = URL(string: model.picture) else { return }
        self.mainImage.load(url: pictureURL)
    }
}

