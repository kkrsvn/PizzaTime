//
//  MVCollectionDelegate.swift
//  PizzaTime
//
//  Created by Kirill Krasavin on 04.04.2023.
//

import UIKit

extension MainViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // MARK: Активация выбранной категории
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoriesCell {
                cell.changeIconColor(forChange: .active)
                
                if indexPath.row < 2 {
                    self.changeScrollViewPosition(at: CGFloat(148 + 172 * indexPath.row * (self.presenter.pizzaDataSource?.recipes.count ?? 0)))
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        // MARK: Деактивация категории
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoriesCell {
                cell.changeIconColor(forChange: .passive)
            }
        } else {
            
        }
    }
}
